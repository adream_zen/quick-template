import $	from 'jquery';


export default function( selector ){
	$(selector +" li").click(function(){
		if( $(this).hasClass("active") ){
			$(this).removeClass("active");
		}else{
			$(".acordeon li").removeClass("active");
			$(this).addClass("active");
		}
	})
}