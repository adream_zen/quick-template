import $	from 'jquery';

export default function(selector="") {
	if(selector.length){
		var selector = selector;
		$(selector).click(function () {
			var _src = $(this).attr("gallery-src");
			$(this).addClass('this');
			$("#image_extender #container").css("background-image", 'url(' + _src + ')');
			$("#image_extender").fadeIn();
		});
		$("#image_extender #exit").click(function () {
			$("#image_extender").fadeOut();
			$(selector + ".this").removeClass('this');
		});
		$("#image_extender #next-ie").click(function () {
			if( $(selector + ".this").next().length ){
				$(selector + ".this").removeClass('this').next().addClass('this');
				var _2 = $(selector + ".this").attr("alt");
				$("#image_extender #container").css("background-image", 'url(' + _2 + ')');
			}
		});
		$("#image_extender #prev-ie").click(function () {
			if( $(selector + ".this").prev().length ){
				$(selector + ".this").removeClass('this').prev().addClass('this');
				var _2 = $( selector +".this").attr("alt");
				$("#image_extender #container").css("background-image", 'url(' + _2 + ')');
			}
		});
	}
}
