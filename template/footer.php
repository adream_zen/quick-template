<?php
/**
 *
 * @package Adream
 */

?>
	<footer class="parent parent--footer">
		<section class="row container row--footer">
			<div class="col col--3">
				<?php img("logo.png") ?>
			</div>
			<div class="col col--3">
				<h4>Strony</h4>
				<ul>
					<li>Home</li>
					<li>Blog</li>
					<li>O nas</li>
					<li>Kontakt</li>
				</ul>
			</div>
			<div class="col col--3">
				<h4>Kontakt</h4>
				<a class="href href--tel" href="#">+48 888 888 888 </a>
				<a href="#" class="href href--mail">biuro@mail.pl</a>
				<p class="href href--adres">ul. Lorem ipsum 22, 34-456 Lorem</p>
			</div>
		</section>
	</footer>
	<div class="parent parent--copyright">
		<section class="row container">
			<div class="col col--2">
				&copy; <?php echo date('Y'); ?>
			</div>
			<col class="col col--2">
				Projekt i wyknonaie <?php img("zensite.png") ?>
			</col>
		</section>
	</div>
</div> 
<?php wp_footer(); ?>
<link rel="stylesheet" id="adream-scss-css" href="<?php echo get_template_directory_uri(); ?>/assets/styles/style.css" type="text/css" media="all">
</body>
</html>