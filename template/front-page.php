<?php get_header(); ?>
<?php
	$args = array( 'post_type' => 'frontpage', 'post_on_page' => -1,'orderby'=>'menu_order', 'order' => 'ASC');
	$loop = new WP_Query( $args ); 
	
	if( $loop->have_posts() ): ?>
	<main class="parent">
		<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
			
			<?php if( has_term( 'background', 'frontpageTax' ) ){ ?>
				<section class="row row--background">
					<div class="container">
						<div class="col col--1">
							<h1 class="border border--bottom"><?php echo get_the_title() ?></h1>
							<h2><?php echo get_the_content(); ?></h2>
						</div>
					</div>
				</section>
			<?php } //endif   ?>

			<?php if( has_term( 'row', 'frontpageTax' ) ){ ?>
				<section class="row container">
					<div class="col col--1">
						<h1 class="border border--bottom"><?php echo get_the_title() ?></h1>
					</div>
						<!-- <p> -->
							<?php echo do_shortcode(get_the_content()); ?>
						<!-- </p> -->
					<!-- </div> -->
				</section>
			<?php } //endif   ?>

			<?php if( has_term( 'counter', 'frontpageTax' ) ){ ?>
				<?php get_template_part("view/front-page/counters") ?>
			<?php } //endif   ?>

		<?php endwhile; ?>
	</main>
<?php endif; ?>
<?php wp_reset_query(); ?>
<?php get_footer(); ?>