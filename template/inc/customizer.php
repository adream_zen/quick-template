<?php
/**
 * Adream Theme Customizer
 *
 * @package Adream
 */

function adream_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
}
add_action( 'customize_register', 'adream_customize_register' );

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function adream_customize_preview_js() {
	wp_enqueue_script( 'adream_customizer', get_template_directory_uri() . '/assets/scripts/customizer.js', array( 'customize-preview' ), '20151215', true );
}
add_action( 'customize_preview_init', 'adream_customize_preview_js' );


function prefix_customizer_register( $wp_customize ) {
	$wp_customize->add_panel( 'panel_parallax', array(
		'priority' => 10,
		'capability' => 'edit_theme_options',
		'title' => __( 'Paralax', 'adream' )
	) );
	/*===========================================
	=            Customizer Parallax            =
	===========================================*/
	
		require get_template_directory() . '/inc/customizer/parallax-1.php';	
	
	/*=====  End of Customizer Parallax  ======*/
	
}
add_action( 'customize_register', 'prefix_customizer_register' );