<?php

// Custom post type counters

if (!function_exists('create_counters_posttype')) {
	function create_counters_posttype() {
		$labels = array(
			'name' => __( 'Liczniki' ),
			'singular_name' => __( 'Licznik' )
		);
		$args = array(
			'labels' 		=> $labels,
			'public' 		=> true,
			'has_archive' 	=> false,
			'hierarchical'	=> true,
			'menu_icon'		=> 'dashicons-performance',
			'supports'		=> array('title','editor','page-attributes'),
			'rewrite' 		=> array('slug' => 'counters'),
		);
		register_post_type( 'counters', $args);
	}
	add_action( 'init', 'create_counters_posttype' );
}
?>