<?php

// Custom post type frontpage

if (!function_exists('create_frontpage_posttype')) {
	function create_frontpage_posttype() {
		$labels = array(
			'name' => __( 'Wersy na stornie głownej' ),
			'singular_name' => __( 'Wers' )
		);
		$args = array(
			'labels' 		=> $labels,
			'public' 		=> true,
			'has_archive' 	=> false,
			'hierarchical'	=> true,
			'menu_icon'		=> 'dashicons-editor-aligncenter',
			'supports'		=> array('title','editor','page-attributes'),
			'rewrite' 		=> array('slug' => 'oferta'),
		);
		register_post_type( 'frontpage', $args);
	}
	add_action( 'init', 'create_frontpage_posttype' );
}
?>