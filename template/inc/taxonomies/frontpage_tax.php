<?php
if (!function_exists('create_frontpageTax')) {

	add_action( 'init', 'create_frontpageTax' );
	function create_frontpageTax() {

		$labels = array(
			'name'              => __( 'Opcje wersu'),
		);

		$args = array(
			'hierarchical'      => true,
			'labels'            => $labels,
			'show_ui'           => true,
			'show_admin_column' => true,
			'query_var'         => true,
			'publicly_queryable'=> true,
			'rewrite'           => array( 'slug' => 'frontpage-tax' ),
		);

		register_taxonomy( 'frontpageTax', 'frontpage' , $args );
	}
}
// $parent_term = term_exists( 'frontpage', 'frontpageTax' ); // array is returned if taxonomy is given
// $parent_term_id = $parent_term['term_id']; // get numeric term id

add_action( 'init', 'custom_terms' );
function custom_terms() {
	$taxonomy = 'frontpageTax';
	$terms = array (
		'0' => array (
			'name'          => 'Wers',
			'slug'          => 'row',
			'description'   => 'Zwykły wers',
			'parent'		=> '',
		),
		'1' => array (
			'name'          => 'Tło',
			'slug'          => 'background',
			'description'   => 'Wers z tłem',
			'parent'		=> '',
		),
		'2' => array (
			'name'          => 'Licznik',
			'slug'          => 'counter',
			'description'   => 'Licznik',
			'parent'		=> '',
		),
	);  

	foreach ( $terms as $term_key=>$term) {
			wp_insert_term(
				$term['name'],
				$taxonomy, 
				array(
					'description'   => $term['description'],
					'slug'          => $term['slug'],
					'parent'		=> $term['parent'],
				)
			);
		unset( $term ); 
	}
}
?>