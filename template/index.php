<?php get_header(); ?>
	<main class="parent">
		<section class="row container">	
			<?php 
				if ( have_posts() ) {
					while ( have_posts() ) {
						the_post(); 
						the_title();
						the_content();
					} // end while
				} // end if
			?>
		</section>
	</main>
<?php get_footer(); ?>