<section class="row row--counters">
	<div class="container">
		<div class="col col--1">
			<h1 class="border border--bottom"><?php echo get_the_title() ?></h1>
		</div>
		<div class="col col--3">
			<div class="count"><?php the_field('variable') ?></div>
			<p>
				<?php the_field('title') ?>
			</p>
		</div>
		<div class="col col--3">
			<div class="count"><?php the_field('variable-3') ?></div>
			<p>
				<?php the_field('title-2') ?>
			</p>
		</div>
		<div class="col col--3">
			<div class="count"><?php the_field('variable-3') ?></div>
			<p>
				<?php the_field('title-3') ?>
			</p>
		</div>
	</div>
</section>