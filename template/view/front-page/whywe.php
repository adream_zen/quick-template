<section class="row container row--whywe">
	<div class="col col--1">
		<h1 class="border border--bottom"><?php echo get_the_title() ?></h1>
	</div>
	<div class="col col--3">
		<i class="fa fa-home" aria-hidden="true"></i>
		<p>
			Lorem ipsum dolor sit amet, consectetur adipisicing elit. Soluta ratione, in, ipsa nam consequuntur laborum dolorum suscipit tempore ea, ducimus eveniet. Qui, fugit exercitationem nulla. Natus et, iusto! Magnam illo dicta odit, asperiores eum, eveniet, soluta, saepe quisquam ab dolor voluptates eaque. Deserunt voluptatum magnam esse, impedit sint adipisci laboriosam pariatur, eligendi ratione quisquam labore veritatis veniam! 
		</p>
	</div>
	<div class="col col--3">
		<i class="fa fa-file-text-o" aria-hidden="true"></i>
		<p>
			 Deserunt voluptatum magnam esse, impedit sint adipisci laboriosam pariatur, eligendi ratione quisquam labore veritatis veniam! Nobis in unde culpa tempore veritatis ipsa sed temporibus ducimus qui possimus, excepturi repellat facilis aspernatur mollitia, a accusantium voluptatum velit soluta veniam. Sit id deleniti soluta, repudiandae optio quod pariatur eius cum totam, excepturi nihil ab maxime tempore atque autem perspiciatis accusamus?
		</p>
	</div>
	<div class="col col--3">
		<i class="fa fa-handshake-o" aria-hidden="true"></i>
		<p>
			Lorem ipsum dolor sit amet, consectetur adipisicing elit. Soluta ratione, in, ipsa nam consequuntur laborum dolorum suscipit tempore ea, ducimus eveniet. Qui, fugit exercitationem nulla. Natus et, iusto! Magnam illo dicta odit, asperiores eum, eveniet, soluta, saepe quisquam ab dolor voluptates eaque. Deserunt voluptatum magnam esse, impedit sint adipisci laboriosam pariatur, eligendi ratione quisquam labore veritatis veniam! Nobis in unde culpa tempore veritatis.
		</p>
	</div>
</section>