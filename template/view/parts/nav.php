<input type="checkbox" id="m_nav">
<div class="header--wrap">
	<div class="logo fixed">
		<a href="<?php echo get_home_url() ?>">
			<img src="<?php echo get_template_directory_uri()?>/assets/img/logo.png">
		</a>
	</div>
	<label for="m_nav" id="mobile-hamburger" onclick>
		<div class="fixed">
			<span></span>
			<span></span>
			<span></span>
			<span></span>
		</div>
	</label>
</div>
<nav>
	<?php wp_nav_menu() ?>
</nav>